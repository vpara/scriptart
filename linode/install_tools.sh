#!/bin/bash

# This updates the packages on the system from the distribution repositories.
sudo apt-get -y update
sudo apt-get -y upgrade

sudo apt-get -y install net-tools jq screen git gzip xclip

sudo apt-get -y install default-jre
sudo apt-get -y install default-jdk

#install anaconda
wget https://repo.continuum.io/archive/Anaconda2-4.3.0-Linux-x86_64.sh &&\
sudo chmod +wrx Anaconda2-4.3.0-Linux-x86_64.sh &&\
bash Anaconda2-4.3.0-Linux-x86_64.sh -b -p $HOME/.anaconda;\
rm -rf Anaconda2-4.3.0-Linux-x86_64.sh;

#update the paths
echo 'PATH=$PATH:~/.anaconda' >> ~/.profile
echo 'JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64' >> ~/.profile
source ~/.profile
conda update anaconda

#install sbt
echo "deb https://dl.bintray.com/sbt/debian-experimental /" | sudo tee -a /etc/apt/sources.list.d/sbt.list
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 642AC823
sudo apt-get update
sudo apt-get install sbt

