#!/bin/bash
HOSTNAME=atlas
FQDN=atlas.asknaboo.com
# This sets the variable $IPADDR to the IP address the new Linode receives.
IPADDR=$(/sbin/ifconfig eth0 | awk '/inet / { print $2 }' | sed 's/addr://')

# This section sets the hostname.
echo $HOSTNAME > /etc/hostname
hostname -F /etc/hostname

# This section sets the Fully Qualified Domain Name (FQDN) in the hosts file.
echo $IPADDR $FQDN $HOSTNAME >> /etc/hosts

echo "alias als='ls -alsh'" >> ~/.profile
thisis=`basename "$0"`
echo  "Removing file $thisis"
rm $thisis

echo 'shell -$SHELL' > ~/.screenrc
